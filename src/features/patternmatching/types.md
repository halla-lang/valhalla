---
name: "Pattern Matching with Types"
---

The function below uses Pattern Matching on a simple Union Type, then returns a different String for each of those different Union Types.

```halla
getSound : Vehicle -> String
getSound vehicle =
    vehicle match
        Car ->
            "vroom"
        Plane ->
            "swoosh"
        Submarine ->
            "sploosh"
```

Using the above function, the result of:
```halla
getSound Plane
```
would be: 
```halla
"swoosh"
```