---
name: "Basics"
todo: true
order: 2
---

Adding Integers together is the same in Halla as it is with most languages.  

```
> 1300 + 37
1337
```

The same could be said for adding Floats.

```
> 13.00 + 00.37
13.37
```