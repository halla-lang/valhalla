---
name: "Math Types"
order: 1
---

There are currently only two types of numbers in Halla. `Int` types for integers, and `Float` types for fractional values. Halla will automatically infer the type for any literal number you provide.

The following REPL command would produce an Integer.
```
> 42
42
```

The following REPL command would produce an Float.
```
> 12.3
12.3
```