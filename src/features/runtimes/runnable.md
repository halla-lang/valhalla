---
name: "Runnable"
order: 1
todo: true
---

Runnable is one the most basic type of Halla programs. In order to create this type of program, simply define that accepts a list of Strings (which represents the arguments the program accepts) and returns a Cmd which the halla runtime will send.

```halla
type Runnable = List String -> Cmd a
```

Using a Runnable, everyone's favorite program "HelloWorld" would look like this:

```halla
helloworld : List String -> Cmd msg
helloworld args =
	Cmd.print "Hello world!"
```