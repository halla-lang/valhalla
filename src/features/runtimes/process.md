---
name: "Actor"
order: 2
todo: true
---

If you need a program that not only capable of sending Cmds, but also *receiving* and acting upon external data, then you need a Actor.

```halla
type Actor msg =
    { init : List String -> Cmd msg
    , publish : msg -> Cmd msgs
    , subscribe : Sub msg
    }
```

Actors are based on the [Actor model](https://en.wikipedia.org/wiki/Carl_Hewitt#Actor_model). Using an Actor is a great way to build a simple application. If your use case is more complex, you can build your application as a series of multiple Actors that work together to solve your problems. Halla makes connecting these Actors together incredibly easy.