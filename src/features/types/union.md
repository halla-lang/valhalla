---
name: "Data Types"
order: 1
---

Here is how you would declare a new type `Vehicle`, which can be either a `Car`, `Plane`, or `Submarine`.

```halla
type Vehicle
    = Car
    | Plane
    | Submarine
```