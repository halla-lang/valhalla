---
name: "Type Parameters"
order: 2
todo: true
---

Union Types can be given parameters to allow them to wrap values. In the example below, the type `Vehicle` has a `Car` case that carries a value of type `Int`.

```halla
type Vehicle
    = Car Int
    | Sled
```

This would mean that to create an instance of type `Car`, you would need to provide it an instance of `Int`:

```
Car 42
```