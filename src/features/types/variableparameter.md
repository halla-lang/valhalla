---
name: "Variable Type Parameters"
order: 3
todo: true
---

Union Types can also be given variable type parameters to allow them to wrap generic values. In the example below, the type `Option` has a `Some` case that carries a value of type `Int`.

```halla
type Option a
    = Some a
    | None
```

This would mean that if you wanted to create an instance of `Option Int`, you could use any of the following expressions:

```halla
Some 42
None
Some 1337
Some 0
```

Similarly, if you wanted to create an instance of `Option String`, you could use any of the following expressions:
```halla
Some "yolo"
None
```