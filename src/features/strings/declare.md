---
name: "Declare String"
order: 1
---
Declare literal Strings just like how you would in most other languages.
```halla
> "helloworld"
"helloworld"
```