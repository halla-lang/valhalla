---
name: "Add Strings Together"
order: 2
---

You can add Strings together using the `+` operator.
```halla
> "hello" ++ "world"
"helloworld"
```

This can be chained as well.
```halla
> "hello" ++ " " ++ "world"
"hello world"
```