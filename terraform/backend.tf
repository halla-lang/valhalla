terraform {
  backend "s3" {
    region = "us-east-1"
    bucket = "all-my-terraform-backends"
    key    = "halla-lang.tfstate"

    encrypt = true
  }
}