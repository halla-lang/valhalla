# This is the ACM (Amazon Certificate Manager) certificate which proves
# to Amazon that we own the domain. This same cert will be used for
# all our TLS needs as well.
resource "aws_acm_certificate" "cloudfront_certificate" {
  # We want a wildcard cert so we can use this for all the different
  # subdomains across our app.
  domain_name = "*.halla-lang.org"

  # set validation method to dns.
  # if you bought the cert from somwhere else you'll probably
  # have to change this to "EMAIL"
  validation_method = "DNS"

  # make the certificate valid for the root domain too.
  # This is optional, so remove it if you want to make sure that
  # Route53 redirects from your root domain DON'T work.
  subject_alternative_names = ["halla-lang.org"]

  provider = "aws.us_east_provider"

  # Tags to track costs.
  tags = {
    Project     = "halla-lang.org"
  }
}