# This s3 bucket exists purely to serve the assets for the website to
# the cloudfront distro. You could host a website outside of AWS if
# you wish too (and then point cloudfront at that domain), but I have
# always loved how easy it is to serve content from S3, so that's what
# I went with for this project.
resource "aws_s3_bucket" "client_source" {
  # Since we are making this website available to cloudfront via
  # S3's static hosting, we could name it pretty much anything we want,
  # as long as it is URI friendly.
  #
  # That being said, I like to name my website source buckets the same
  # as the cloudfront domain they provide assets to, so that's what I'm
  # doing here.
  bucket = "halla-lang.org"

  # Makes the website public to any services that use ACLs to view content,
  # Since cloudfront is using a role, you could turn this off if you wish,
  # but since I WANT anyone to be able to look at the content of this bucket,
  # I'm just gonna open it to everyone.
  acl = "public-read"

  # This option informs terraform that when we want to destroy the website
  # (i.e. when we run `terraform destroy`), terraform should attempt
  # remove the s3 bucket, regardless of the fact that it has objects in it.
  force_destroy = true

  # Configure hosting properties of the website
  website {
    # This is the key of the object we will show to anyone accessing the service
    # through the url directly. This only exists to allow users to access the
    # bucket DIRECTLY through S3 Static Hosting, rather than through CloudFront.
    #
    # If you want to only host through CloudFront, you could remove this.
    index_document = "index.html"

    # Key of the object that we will show when anyone tries to access
    # this S3 bucket directly though S3 Static Hosting and something
    # goes wrong.
    error_document = "error.html"
  }

  # Tags to track costs.
  tags = {
    Project     = "halla-lang.org"
    ServiceType = "ui"
  }

  # Makes the website public to more modern services, like CloudFront for
  # instance. If you wanted to ONLY allow cloudfront to fetch objects
  # from the bucket, you could set the "Principal" of this policy accordingly.
  # Again though, I wanna keep this bucket as open as possible.
  policy = <<POLICY
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Sid":"AddPerm",
      "Effect":"Allow",
      "Principal": "*",
      "Action":["s3:GetObject"],
      "Resource":["arn:aws:s3:::halla-lang.org/*"]
    }
  ]
}
POLICY
}