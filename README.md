# Valhalla
###### Website and Information Dump for the Halla Language

This repo is the home of the source for the Halla website, hosted at [halla-lang.org](https://www.halla-lang.org). It uses Terraform to provision resources.

## Cookbook

- All cookbook actions assume you are using Node v10.10.0.

### Develop

You can start a Node.js development server using:

```bash
npm run serve
```

The server should spin up shortly and you will be able to see the website hosted at [localhost:8080](localhost:8080). The server will watch your local files for updates (aka livereload) to help make updating the website quick and easy.

### Deploy

Assuming you have adequate AWS permissions, you can use the following helper script. 

```
NODE_ENV=production npm run deploy
```

This script will build the application, then copy it to S3. The next time the Cloudfront cache is busted, any changes you've made should show up on the website.