var Metalsmith  = require('metalsmith');
var collections = require('metalsmith-collections');
var inPlace     = require('metalsmith-in-place');
var layouts     = require('metalsmith-layouts');
var permalinks  = require('metalsmith-permalinks');
var sass = require('metalsmith-sass');
const metalsmithCleanCSS = require('metalsmith-clean-css')
const concat = require('metalsmith-concat')
var htmlMinifier = require("metalsmith-html-minifier");
var serve = require('metalsmith-serve');
var watch = require('metalsmith-watch');
var built = false;

const buildDestination = function(env) {
    if (env === 'production') {
        console.warn("Creating production build.");
        return "./dist";
    }

    return "./build";
}(process.env.NODE_ENV);

const baseApplication = function() {
    return Metalsmith(__dirname)
      // show where source comes from
      .source('./src')
      .destination(buildDestination)
      .clean(false)
      // add plugins
      .use(collections({
        features: {
          pattern: "features/*.md",
          sortBy: "order"
        },
        runtimes: {
          pattern: "features/runtimes/*.md",
          sortBy: "order"
        },
        patternmatching: {
          pattern: "features/patternmatching/*.md",
          sortBy: "order"
        },
        strings: {
          pattern: "features/strings/*.md",
          sortBy: "order"
        },
        math: {
          pattern: "features/math/*.md",
          sortBy: "order"
        },
        types: {
          pattern: "features/types/*.md",
          sortBy: "order"
        }
      }))
      .use(permalinks())
      .use(inPlace({
        engineOptions: {
          // highlight: code => require('highlight.js').highlightAuto(code).value
        }
      }))
      .use(layouts())
      .use(sass({
        outputDir: 'css/'
      }))
      .use(metalsmithCleanCSS({
        files: '**/*.css',
        cleanCSS: {
          rebase: true,
        },
      }))
      .use(concat({
        files: 'css/**/*.css',
        output: 'css/main.css',
        forceOutput: true
      }))
      .use(htmlMinifier({
        pattern: "**/*.html",
      }))
};


// if this is a production build, build the app
if (process.env.NODE_ENV === "production") {
    return baseApplication()
      .build(function(err, files) {
        if (err) { console.error(err); }
      });;
}

console.warn("Using Development Mode...")

// serve what is compiled with live reload to make development easy
baseApplication()
  .metadata({
    environment: "development"
  })
  .use(serve({
    host: "0.0.0.0"
  }))
  .use(watch({
    paths: {
      "${source}/**/*": "**/*",
      "layouts/**/*": "**/*"
    },
    livereload: true
  }))
  .build(function(err, files) {
     if (err) { console.error(err); }
  });
